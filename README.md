# keycloak
## Overview
Goal of this project is to build the keycloak server using the docker and MySQL DB.
The `docker-compose.yml` helps us to build the desired environment.

All images used here are the latest images. I have provided the specific version below used while testing

 - Keycloak - 13.0.0 (The OAuth server)
 - MySQL - 8.0.25 (DB for Keycloak)
 - Adminer - 4.8.0 (DB visualization tool for MySQL)

**_NOTE_**
```
The credentials for keycloak and MySQL as mentioned as flat text in docker-compose.yml. This approach is used only
for testing and not for production use.
Please refer the respective documentionation to supply them as secrets.
```
> Adminer image is profiled with name `debug` as it is not mandatory for Keycloak setup.
> If you need to look into the MySQL then only run this image with `--profile` option

## Useful Info
 - Start the containers - `docker compose up -d`
   - This will start all the containers with detached (`-d`) mode except Adminer
   - To run Adminer image use `docker compose --profile debug up -d`
   - Will create a network (`keycloak-net`) to operate all containers in single network
   - Will create a named volume (`db_data`) for MySQL
 - Stop the containers - `docker compose down`
   - Will bring all the containers down and removed them
   - The volume will be retained the used during the next run so that the data is not lost
    
## Testing Instruction
Once all the containers are up the services runs on the following host ports:
 - Keycloak - `8080`
 - MySQL - `3306`
 - Adminer - `8090`

From your browser, hit the below URLs:

**Keycloak**
>http://localhost:8080/  
>This will open the keycloak management console.
>Use `KEYCLOAK_USER` and `KEYCLOAK_PASSWORD` mentioned in the yml file to log in

```
Please refer keycloak documentation for further actions such as realm creation, user creation etc.
```
**Adminer**
>http://localhost:8090/  
>This will open the Adminer console to manage MySQL.
>User `mysql` as server, `root` as user and password as mentioned (`MYSQL_ROOT_PASSWORD`) in the yml file
